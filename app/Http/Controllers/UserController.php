<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $data = User::get();
        return view('user.index', compact('data'));
    }
    public function add()
    {
        return view('user.create');
    }
    public function store(Request $request)
    {
        $data = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => $request->password,
        ]);
        return back();
    }
    public function edit($id)
    {
        $edit = User::where('id', $id)->first();
        return view('user.edit')->with('edit', $edit);
    }
    public function update(Request $request, $id)
    {
        $update = user::where("id", $id)->update([
            'name' => $request->name,
            'username' => $request->username,
            'password' => $request->password,
        ]);

        return back()->withMessage('Success Update');
    }
}
