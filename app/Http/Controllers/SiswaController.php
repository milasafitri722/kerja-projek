<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\Imports\SiswaImport;
use Excel;

class SiswaController extends Controller
{
    public function index()
    {
        return view('siswa.index',[
            'data' => Siswa::get(),
        ]);
    }
    public function importExcel() 
    {
        Excel::import(new SiswaImport,request()->file('file'));
           
        return back();
    }
}
