<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('nis');
            $table->String('nama');
            $table->String('rombel');
            $table->String('rayon');
            $table->String('item_name');
            $table->String('item_size');
            $table->String('merk');
            $table->String('tempat');
            $table->Integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventors');
    }
}
