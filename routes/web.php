<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LaporanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index')->middleware('HomeMiddleware');
Route::post('login', 'LoginController@login');
Route::get('logout', 'LoginController@logout');
Route::get('/table', 'LoginController@table');
Route::get('/home', 'HomeController@index')->name('home')->middleware('LoginMiddleware');

Route::get('get-data/{id}', 'InventorController@getData');

Route::group(['prefix' => 'barang'], function () {
    Route::get('/', ['as' => 'barang', 'uses' => 'BarangController@index']);
    Route::get('/selesai', 'BarangController@selesai')->name('barang.selesai');
    Route::get('/take/{id}', 'BarangController@take')->name('barang.take');
    Route::post('/take_owner/{id}', 'BarangController@take_owner')->name('barang.take_owner');
    Route::delete('/delete/{id}', 'BarangController@destroy')->name('barang.destroy');
    Route::get('/edit/{id}', 'BarangController@edit')->name('barang.edit');
    Route::PATCH('/update/{id}', 'BarangController@update')->name('barang.update');
});

Route::group(['prefix' => 'inventor'], function () {
    Route::get('/', ['as' => 'inventor', 'uses' => 'InventorController@index']);
    Route::post('/post', 'InventorController@store')->name('inventor.store');
});

Route::group(['prefix' => 'siswa'], function () {
    Route::get('/', ['as' => 'siswa', 'uses' => 'SiswaController@index']);
    Route::post('/import', 'SiswaController@importExcel')->name('siswa.importExcel');
});

Route::group(['prefix' => 'laporan'], function () {
    Route::get('/', 'LaporanController@index')->name('laporan');
    Route::post('/create', 'LaporanController@tanggal')->name('report.tanggal');
});
Route::get('user', 'UserController@index');
Route::get('user/add', 'UserController@add');
Route::post('user/store', 'UserController@store');
Route::get('/edit/profile/{id}', 'UserController@edit');
Route::patch('/user/update/{id}', 'UserController@update');
