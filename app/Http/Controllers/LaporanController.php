<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventor;


class LaporanController extends Controller
{
    public function index(){
        return view('laporan.index');
    }
    public function tanggal(Request $request)
    {
        return view('laporan.tanggal',[
            'data'=>Inventor::whereBetween('created_at',[$request->date_first,$request->date_last])->latest()->get(),
            'date_first'=>$request->date_first,
            'date_last'=>$request->date_last,
        ]);
    }
}
