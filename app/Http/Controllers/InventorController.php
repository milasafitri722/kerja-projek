<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\Inventor;

class InventorController extends Controller
{
    public function index(){
        return view('inventor.index');
    }
    public function getData($id)
    {
        $siswa = Siswa::where('nis',$id)->first();
        return json_encode($siswa);
    }
    public function store(Request $request){
       Inventor::create([
        'nis' => $request->nis,
        'nama' => $request->nama,
        'rombel' => $request->rombel,
        'rayon' => $request->rayon,
        'item_name' => $request->item_name,
        'item_size' => $request->item_size,
        'merk' => $request->merk,
        'tempat' => $request->tempat,
        'status' => 1,
       ]);
        return redirect()->route('barang');
    }
}