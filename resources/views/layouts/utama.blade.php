<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIM BARANG HILANG</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('dist/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">


  <link rel="stylesheet"
    href="{{ asset('dist/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css">

  <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-tokenfield.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-tokenfield.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/tokenfield-typeahead.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/tokenfield-typeahead.min.css') }}">

  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
      .buttons-excel{
        background-color: #00BFFF !important;
        color:white;
      }
      .buttons-pdf{
        background-color: #00BFFF !important;
        color:white;
      }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <header class="main-header">
      <a href="" class="logo">
        <span class="logo-mini"><b>D</b>P</span>
        <span class="logo-lg"><b>SIM Barang</b>Hilang</span>
      </a>
      <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{ asset('image/user.png') }}" class="user-image" alt="User Image">
                <span class="hidden-xs">{{$nama_login->name}}</span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header">
                  <img src="{{ asset('image/user.png') }}" class="img-circle" alt="User Image">
                  <p>
                    {{ $nama_login->name }}
                    <small></small>
                  </p>
                <li class="user-footer">
                  <div class="pull-left">
                  <a href="{{ url('edit/profile', $nama_login->id)}}" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="{{ url('/logout')}}" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
            </li>
          </ul>
          </li>
          </ul>
        </div>
      </nav>
    </header>
    <aside class="main-sidebar">
      <section class="sidebar">
        <div class="user-panel">
          <div class="pull-left image">
            <img src="{{ asset('image/user.png') }}" style="width:60px; height:50px; BORDER-RADIUS:50%;">
          </div>
          <div class="pull-left info">
            <p>{{ $nama_login->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
          </div>
        </form>
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">HEADER</li>
          <li class="active"><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
          <li><a href="{{ route('inventor') }}"><i class="fa fa-inbox"></i> <span>Input Barang Hilang</span></a></li>
          <li><a href="{{ route('barang') }}"><i class="fa fa-fw fa-folder-open-o"></i> <span>Data Barang
                Hilang</span></a></li>
          <li><a href="{{ route('barang.selesai') }}"><i class="fa fa-tasks"></i> <span>Data Barang
                Selesai</span></a></li>
          <li><a href="{{ route('siswa') }}"><i class="fa fa-edit"></i> <span>Data Siswa</span></a></li>
          <li><a href="{{route('laporan')}}"><i class="fa fa-fw fa-history"></i> <span>Laporan</span></a></li>
          <li><a href="{{ url('user')}}"><i class="fa fa-fw fa-history"></i> <span>User</span></a></li>
      </section>
    </aside>
    <div class="content-wrapper">
      <section class="content-header">
      </section>
      <section class="content container-fluid">
        @yield('content')
      </section>
    </div>
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        SMK WIKRAMA BOGOR
      </div>
      <strong>Copyright &copy; 2019 <a href="#">SIM BARANG HILANG</a>.</strong>
    </footer>
    <aside class="control-sidebar control-sidebar-dark">
      <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
      </ul>
    </aside>
    <div class="control-sidebar-bg"></div>
  </div>



  <script src="{{ asset('dist/bower_components/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('dist/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('dist/dist/js/adminlte.min.js') }}"></script>
  <script src="{{ asset('dist/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('dist/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('dist/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ asset('dist/bower_components/fastclick/lib/fastclick.js') }}"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
  <script src="{{ asset('dist/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('dist/js/bootstrap-tokenfield.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>

  @yield('js')
  <!-- AdminLTE App -->
  <script>
    $(document).ready(function () {
      $('.tokenfield').tokenfield();

      var table = $('#example').DataTable({
        lengthChange: false,
        buttons: ['excel', 'pdf']
      });

      table.buttons().container()
        .appendTo('#example_wrapper .col-sm-6:eq(0)');
    });
  </script>
  <script>
    $(function () {
      $('#example1').DataTable()
      $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
      })
    })
  </script>

  <script>
    $('.dropify').dropify();
  </script>

  <script>
    $('.datepicker').datepicker({
      autoclose: true,
      format: "yyyy",
      viewMode: "years",
      minViewMode: 2,
      // minViewMode: "years"
    });
  </script>

</body>

</html>
