@php
$nama_login = DB::table('users')->where('id',session('user_id'))->first();
@endphp
@extends('layouts.utama')
@section('title','Data Barang')
@section('content')
<div class="box box-info">
<div class="box-body">
<table id="example1" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>NIS</th>
                <th>Name</th>
                <th>Rombel</th>
                <th>Rayon</th>
                <th>Item Name</th>
                <th>Size</th>
                <th>Merk</th>
                <th>Tempat</th>
                <th>Status</th>
                <th>ACTION</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $a)
            <tr>
                <td>{{ $loop->index+1 }}</td>
                <td>{{ $a->nis }}</td>
                <td>{{ $a->nama }}</td>
                <td>{{ $a->rombel }}</td>
                <td>{{ $a->rayon }}</td>
                <td>{{ $a->item_name }}</td>
                <td>{{ $a->item_size }}</td>
                <td>{{ $a->merk }}</td>
                <td>{{ $a->tempat }}</td>
                <td>
                    @if($a->status == 1)
                    <div class="budges">
                         <a href="{{ route('barang.take', $a['id'])}}"><span class="badge badge-danger">Process</span></a>
                    </div>
                    @else
                    <div class="budges">
                         <span class="badge badge-success">Done</span>
                    </div>
                    @endif
                </td>
                <td>
                <form action="{{ route('barang.destroy', $a->id) }}" method="post">
                    @csrf @method('delete')
                        <div class="buttons">
                        <button class="btn btn-danger" onclick="return confirm('Are You Sure?')"><i class=" fa fa-trash"></i></button>
                        <a href="{{ route('barang.edit', $a['id']) }}" class="btn btn-warning"><i class=" fa fa-edit"></i></a>
                        </div>
                </form>
                </td>
            </tr>
            @endforeach
        </tbody> 
    </table>
</div>
</div>
@endsection