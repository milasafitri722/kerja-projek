@php
$nama_login = DB::table('users')->where('id',session('user_id'))->first();
@endphp
@extends('layouts.utama')
@section('content')

<div class="col-md-8 col-md-offset-2">
    <div class="box box-info">
        <div class="box-header with-border">
            <i class="fa fa-pencil">
                <h1 class="box-title">Print Barang</h1>
            </i>
        </div>
        <form method="post" name="myform" action="{{ route('report.tanggal') }}">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Date Start</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="date_first">
                                </div>
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Date End</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="date_last">
                                </div>
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                        <div class="col-sm-12">
                            <div class="card-footer pull-right">
                                <button class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                            </div>
                        </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>

@endsection