<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventor;
use App\Owner;

class BarangController extends Controller
{
    public function index(){
        return view('barang.index',[
            'data' => Inventor::where('status',1)->get(),
        ]);
    }
    public function selesai(){
        return view ('barang.barangselesai',[
            'data' => Inventor::where('status', 2)->get(),
        ]);
    }
    public function take($id)
    {
        $take = Inventor::where('id',$id)->first();
        return view('barang.ambilbarang')->with('take',$take);
    }
    public function take_owner(Request $request, $id)
    {
        Owner::create([
            'nis' => $request->nis,
            'nama' => $request->nama,
            'rombel' => $request->rombel,
            'rayon' => $request->rayon,
            'item_name' => $request->item_name,
            'item_size' => $request->item_size,
            'merk' => $request->merk,
            'status' => 2,
           ]);
           $take = Inventor::where('id', $id)->update([
               'status' => 2,
           ]);
            return redirect()->route('barang');
    }
    
    public function destroy($id)
    {
        $delete = Inventor::where("id", $id)->delete();
        if ($delete){
            return redirect()->route('barang')->with('danger','Berhasil di hapus!!');
        } else{
            return redirect()->route('barang');
        }
    }
    public function edit($id)
    {
       $edit = Inventor::where('id', $id)->first();
       return view('barang.edit')->with('edit', $edit);
    }
    
    public function update(Request $request, $id)
    {
        $edit = Inventor::where("id", $id)->update([
            'nis' => $request->nis,
            'nama' => $request->nama,
            'rombel' => $request->rombel,
            'rayon' => $request->rayon,
            'item_name' => $request->item_name,
            'item_size' => $request->item_size,
            'merk' => $request->merk,
            'tempat' => $request->tempat,
            'status' => 1,
        ]);

        return redirect()->route('barang')->withMessage('Success Update');
    }
}

