@php
$nama_login = DB::table('users')->where('id',session('user_id'))->first();
@endphp
@extends('layouts.utama')
@section('title','Data Barang')
@section('content')
<div class="box box-info">
    <div style="margin-bottom: 20px !important; margin-top: 20px !important; margin-right: 10px !important;">
    <a href="{{ url('user/add') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Data</a>
        <br>
    </div>
    <div class="box-body">
        <table id="example1" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Usernama</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $a)
                <tr>
                    <td>{{ $loop->index+1 }}</td>
                    <td>{{ $a->name }}</td>
                    <td>{{ $a->username }}</td>
                    <td>
                        <form action="" method="post">
                            @csrf @method('delete')
                            <div>
                                @if($nama_login->id != $a->id)
                                 <button class="btn btn-danger" onclick="return confirm('Are You Sure?')"><i
                                        class=" fa fa-trash"></i></button>
                                @endif
                        </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
