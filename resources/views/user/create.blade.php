
@php
$nama_login = DB::table('users')->where('id',session('user_id'))->first();
@endphp
@extends('layouts.utama')
@section('content')

<div class="col-md-10 col-md-offset-1">
    <div class="box box-info">
        <div class="box-header with-border">
            <i class="fa fa-pencil">
                <h1 class="box-title">Input Barang Hilang</h1>
            </i>
        </div>
    <form method="post" name="myform" action="{{url('user/store')}}">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" placeholder="Name"
                                    name="name">
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" placeholder="username"
                                    name="username">
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="inputEmail3" placeholder="Password" name="password">
                            </div>
                        </div>
                        <div class="box-footer pull-right">
                            <button type="submit" class="btn btn-danger">Cancel</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>
@endsection
