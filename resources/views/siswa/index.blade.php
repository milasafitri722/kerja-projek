@php
$nama_login = DB::table('users')->where('id',session('user_id'))->first();
@endphp
@extends('layouts.utama')
@section('title','Data Siswa')
@section('content')
<div class="box box-info">
<div class="box-body">
    <table id="example1" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>NIS</th>
                <th>Name</th>
                <th>Rombel</th>
                <th>Rayon</th>
                <th>JK</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $a)
            <tr>
                <td>{{ $a->id }}</td>
                <td>{{ $a->nis }}</td>
                <td>{{ $a->nama }}</td>
                <td>{{ $a->rombel }}</td>
                <td>{{ $a->rayon }}</td>
                <td>
                    @if($a->jk == 1)
                    Perempuan
                    @else
                    Laki-Laki
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
@endsection
