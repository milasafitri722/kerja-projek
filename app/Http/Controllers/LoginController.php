<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\User;

class LoginController extends Controller
{
    public function index(){
        return view('login');
    }
    public function login(Request $request)
    {
        $cek = User::Where([
            'username' => $request->username,
            'password' => $request->password,
        ]);
        if($cek->count() > 0){
            session::put('user_id',$cek->first()->id);
            return redirect('home');
        }else{
            return back()->with('danger','Data Invalid !!');
        }
    }

    public function table()
    {
        return view('table');
    }
    public function logout(){
        session::flush();
        return redirect('');
    }
}
