@php
$nama_login = DB::table('users')->where('id',session('user_id'))->first();
@endphp
@extends('layouts.utama')
@section('content')

<div class="col-md-10 col-md-offset-1">
    <div class="box box-info">
        <div class="box-header with-border">
            <i class="fa fa-pencil">
                <h1 class="box-title">Input Barang Hilang</h1>
            </i>
        </div>
        <form method="post" name="myform" action="{{ route('inventor.store') }}">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">NIS</label>
                            <div class="col-sm-10">
                                <div class="input-group input-group">
                                    <input type="text" class="form-control" name="nis" id="nis">
                                    <span class="input-group-btn">
                                        <button type="button" onclick="carinis()"
                                            class="btn btn-info btn-flat">Cari</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" id="nama" class="form-control" id="inputEmail3" placeholder="Nama" name="nama">
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Rombel</label>
                            <div class="col-sm-10">
                                <input type="text" id="rombel" class="form-control" id="inputEmail3" placeholder="Rombel"
                                    name="rombel">
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Rayon</label>
                            <div class="col-sm-10">
                                <input type="text" id="rayon" class="form-control" id="inputEmail3" placeholder="Rayon"
                                    name="rayon">
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Item Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" placeholder="Item Name"
                                    name="item_name">
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Item Size</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" placeholder="Item Size"
                                    name="item_size">
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Merk</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" placeholder="merk" name="merk">
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Tempat Menemukan</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="inputEmail3" placeholder="Tempat Menemukan"
                                    name="tempat"></textarea>
                            </div>
                        </div>
                        </br>
                        </br>
                        <div class="box-footer pull-right">
                            <button type="submit" class="btn btn-danger">Cancel</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    function carinis() {
        $.ajax({
            url: "{{url('get-data')}}/" + $("#nis").val(),
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {
                $("#nama").val(data.nama);
                $("#rayon").val(data.rayon);
                $("#rombel").val(data.rombel);
            },
            error: function (x) {
                alert(x);
            }
        });
    }
</script>
@endsection