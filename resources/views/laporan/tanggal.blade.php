@php
$nama_login = DB::table('users')->where('id',session('user_id'))->first();
@endphp
@extends('layouts.utama')
@section('title','Data Barang')
@section('content')
<h2>Preview Total Data Barang</h2>
<div class="box box-info">
<div class="box-body">
<table id="example" class="table table-striped table-bordered" style="width:100%">
<center><img src="{{ asset('image/wikrama.png') }}" class="user-image" alt="User Image" width="70px" ><br/>
Jl. Raya Wangun, Bogor Timur,  <br> Sindangsari,Bogor Tim., Kota Bogor, Jawa Barat 16146<br>
Tanggal : {{$date_first}} Sampai {{$date_last}}
</center><br>
        <thead>
            <tr>
                <th>ID</th>
                <th>NIS</th>
                <th>Name</th>
                <th>Rombel</th>
                <th>Rayon</th>
                <th>Item Name</th>
                <th>Size</th>
                <th>Merk</th>
                <th>Tempat</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $a)
            <tr>
                <td>{{ $loop->index+1 }}</td>
                <td>{{ $a->nis }}</td>
                <td>{{ $a->nama }}</td>
                <td>{{ $a->rombel }}</td>
                <td>{{ $a->rayon }}</td>
                <td>{{ $a->item_name }}</td>
                <td>{{ $a->item_size }}</td>
                <td>{{ $a->merk }}</td>
                <td>{{ $a->tempat }}</td>
                <td>
                    @if($a->status == 1)
                    <div class="budges">
                         <a href="{{ route('barang.take', $a['id'])}}"><span class="badge badge-danger">Process</span></a>
                    </div>
                    @else
                    <div class="budges">
                         <span class="badge badge-success">Done</span>
                    </div>
                    @endif
                </td>
                
            </tr>
            @endforeach
        </tbody> 
    </table>
</div>
</div>
@endsection